﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AnimPlugin;

[RequireComponent(typeof(CanvasGroup))]
public class Menu : MonoBehaviour {
	#region EVENTS

	#endregion

	#region MEMBERS
	const float ANIM_TIME = .7f;
	const int GAME_TO_POINTS = 3;

	[ContextMenuItem("show", "Show")]
	[ContextMenuItem("hide", "Hide")]
	public int testComm;

	[Header("references")]
	[SerializeField] Paddle leftPaddle;
	[SerializeField] Paddle rightPaddle;

	[SerializeField] Text pointsLeftText;
	[SerializeField] Text pointsRightText;
	[SerializeField] Text playerWonText;

	[Header("prefabs")]
	[SerializeField] Ball ballPrefab;

	int _pointsLeft;
	int _pointsRight;
	#endregion

	#region PROPERTIES
	public static Menu Instance { get; private set; }
	bool IsDemo {
		get; set;
	}

	int PointsLeft {
		get { return this._pointsLeft; }
		set {
			this._pointsLeft = value;
			this.pointsLeftText.text = value.ToString();
		}
	}

	int PointsRight {
		get { return this._pointsRight; }
		set {
			this._pointsRight = value;
			this.pointsRightText.text = value.ToString();
		}
	}

	CanvasGroup CachedCanvasGroup { get; set; }

	#endregion

	#region METHODS
	void Awake() {
		Menu.Instance = this;
		CachedCanvasGroup = this.GetComponent<CanvasGroup>();
		// demo
		Demo();
	}

	void Start() {
		ResetGame();
	}

	void Demo() {
		IsDemo = true;
		this.leftPaddle.AIDriven = true;
		this.rightPaddle.AIDriven = true;
		ResetGame();
		NextBall();
	}

	void ResetGame() {
		if(!IsDemo) {
			PointsLeft = 0;
			PointsRight = 0;
			this.playerWonText.gameObject.SetActive(false);
		}
		if(Ball.CurrentBallReference != null) {
			GameObject.Destroy(Ball.CurrentBallReference.gameObject);
		}
	}

	void NextBall() {
		this.StartCoroutine(
			Anim.Chain(
				Anim.Wait(ANIM_TIME),
				Anim.Void( () => {GameObject.Instantiate(ballPrefab, null, false); } )
			)
		);
	}

	public void StartOnePlayerBtnClicked() {
		IsDemo = false;
		ResetGame();
		this.leftPaddle.AIDriven = false;
		this.rightPaddle.AIDriven = true;
		NextBall();
		Hide();
	}

	public void StartTwoPlayersBtnClicked() {
		IsDemo = false;
		ResetGame();
		this.leftPaddle.AIDriven = false;
		this.rightPaddle.AIDriven = false;
		NextBall();
		Hide();
	}

	public void ExitBtnClicked() {
		Application.Quit();
	}

	public void Show() {
		this.StartCoroutine(
			Anim.Parallel(
				Anim.PerformFunction(SetAlpha, 0f, 1f, ANIM_TIME, Ease.SineIn),
				Anim.ScaleTo(this.transform, Vector3.one, ANIM_TIME, Ease.SineIn)
			)
		);
		CachedCanvasGroup.interactable = true;
	}

	public void Hide() {
		this.StartCoroutine(
			Anim.Parallel(
				Anim.PerformFunction(SetAlpha, 1f, 0f, ANIM_TIME, Ease.SineOut),
				Anim.ScaleTo(this.transform, Vector3.one * 7f, ANIM_TIME, Ease.SineOut)
			)
		);
		CachedCanvasGroup.interactable = false;
	}

	void SetAlpha(float a) {
		this.CachedCanvasGroup.alpha = a;
	}

	public void AddPoints(WHOSE whose) {
		switch(whose) {
			case WHOSE.blue:
				this.PointsLeft++;
				break;
			case WHOSE.red:
				this.PointsRight++;
				break;
		}

		if(this.CheckWinningConditions()) {
			this.playerWonText.gameObject.SetActive(true);
			this.Show();
			Demo();
		} else {
			NextBall();
		}
	}

	bool CheckWinningConditions() {
		if(PointsLeft >= GAME_TO_POINTS) {
			this.playerWonText.text = "Blue Player Won!";
		} else if (PointsRight >= GAME_TO_POINTS) {
			this.playerWonText.text = "Red Player Won!";
		}
		return !IsDemo && (PointsLeft >= GAME_TO_POINTS || PointsRight >= GAME_TO_POINTS);
	}
	#endregion
}
