﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ExitTrigger : MonoBehaviour {

	#region EVENTS

	#endregion

	#region MEMBERS
	[SerializeField] WHOSE whose;
	#endregion

	#region PROPERTIES

	#endregion

	#region METHODS
	void OnTriggerEnter2D(Collider2D collider) {
		Menu.Instance.AddPoints(whose);
		GameObject.Destroy(Ball.CurrentBallReference);
	}
	#endregion
}

public enum WHOSE {
	blue,
	red
}