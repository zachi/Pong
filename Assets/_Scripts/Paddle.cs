﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
	
	#region EVENTS

	#endregion

	#region MEMBERS
	const float SPEED = 4.5f;
	const float MIN_MAX = 4.3f;

	[SerializeField] KeyCode kcUp;
	[SerializeField] KeyCode kcDown;
	#endregion

	#region PROPERTIES
	public float VelocityY { 
		get {
			return currFrameY - prevFrameY;
		}
	}

	public bool AIDriven {
		get; set;
	}

	float prevFrameY;
	float currFrameY;
	#endregion

	#region METHODS

	void FixedUpdate() {
		prevFrameY = currFrameY;
		if(AIDriven) {
			ProcessAIDriven();
		} else {
			ProcessInputDriven();
		}
		if(Mathf.Abs(this.transform.position.y) > MIN_MAX) {
			this.transform.position = new Vector3(
				this.transform.position.x,
				Mathf.Sign(this.transform.position.y) * MIN_MAX,
				this.transform.position.z
			);
		}
		currFrameY = this.transform.position.y;
	}

	void ProcessInputDriven() {
		if (Input.GetKey(kcUp)) {
			this.transform.position += Vector3.up * Time.fixedDeltaTime * SPEED;
		} else if (Input.GetKey(kcDown)) {
			this.transform.position += Vector3.down * Time.fixedDeltaTime * SPEED;
		}
	}

	void ProcessAIDriven() {
		if(Ball.CurrentBallReference != null) {
			float tgtY = Ball.CurrentBallReference.transform.position.y;
			if(tgtY > this.transform.position.y) {
				this.transform.position += Vector3.up * Time.fixedDeltaTime * SPEED;
			} else {
				this.transform.position += Vector3.down * Time.fixedDeltaTime * SPEED;
			}
		}
	}

	#endregion
}
