﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
	#region EVENTS

	#endregion

	#region MEMBERS
	public static Ball CurrentBallReference { get; private set; }
	#endregion

	#region PROPERTIES
	Rigidbody2D CachedBody { get; set; }
	bool UseRotation { get; set; }
	float RotationMod { get; set; }
	float YMod { get; set; }
	#endregion

	#region METHODS
	void Awake()
	{
		this.CachedBody = this.GetComponent<Rigidbody2D>();
		Ball.CurrentBallReference = this;
	}

	void Start()
	{
		
		this.CachedBody.AddForce(new Vector2(120f, Random.Range(-40f, 40f)));
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player") {
			UseRotation = true;
			RotationMod = collision.gameObject.GetComponent<Paddle>().VelocityY;
			YMod = this.transform.position.y - collision.gameObject.transform.position.y;
		} else {
			UseRotation = false;
			RotationMod = 0f;
			YMod = 0f;
		}
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		if(UseRotation) {
			float power = Mathf.Min(this.CachedBody.velocity.magnitude + .05f, 9f); 
			this.CachedBody.velocity = new Vector2(this.CachedBody.velocity.x, this.YMod * 5f).normalized * power;
		}
	}

	void FixedUpdate() {
		this.CachedBody.AddForce(-RotationMod * Vector2.up * 1200f * Time.fixedDeltaTime);
	}

	#endregion
}
